"use strict";

/* 
photo_details is an array of objects. Using the usual array indexing notation you can access 
an object, and you can access each property of an object by name from there.

    Retrieve an object:
        photo_details[i]

    Accessing an object property:
        photo_details[i].filename
        photo_details[i].filesize
*/
var photo_details = [
    { filename: "christchurch8105352.jpeg", filesize: 1642621 }
  , { filename: "christchurch8105356.jpeg", filesize: 2170021 }
  , { filename: "christchurch8105406.jpg", filesize: 2252274 }
  , { filename: "christchurchcathedral8105329.jpg", filesize: 936631 }
  , { filename: "christchurchchathedral8105387.jpg", filesize: 1047573 }
  , { filename: "christchurchpress8105370.jpg", filesize: 898708 }
  , { filename: "christchurchpress8105372.tif", filesize: 10931729 }
  , { filename: "christchurchurchartgallery8105441.jpg", filesize: 1104666 }
  , { filename: "christchurchurchartgallery8105444.jpg", filesize: 1199118 }
  , { filename: "christchurchurchartgallery8105448.jpg", filesize: 695987 }
  , { filename: "queenstown8135644.jpg", filesize: 2290009 }
  , { filename: "queenstown8135665.jpg", filesize: 911133 }
  , { filename: "queenstown8135678.jpg", filesize: 812770 }
  , { filename: "queenstownjetty8135650.jpg", filesize: 1564584 }
  , { filename: "remarkables8135623.jpg", filesize: 599296 }
  , { filename: "remarkables8135627.jpg", filesize: 668989 }
  , { filename: "remarkables8145706.jpg", filesize: 936446 }
  , { filename: "remarkables8145731.tif", filesize: 925865 }
  , { filename: "remarkables8155745.jpg", filesize: 1189581 }
  , { filename: "remarkables8155746.jpg", filesize: 1079125 }
  , { filename: "remarkables8155750.jpg", filesize: 1014981 }
  , { filename: "remarkables8155753.jpg", filesize: 1191699 }
  , { filename: "remarkables8155754.jpg", filesize: 1088554 }
  , { filename: "remembrancebridge8105347.jpg", filesize: 830057 }
  , { filename: "remembrancebridge8105349.tiff", filesize: 10933160 }
];


function isWebImageFriendly(filename)
{
    // Rewrite this function so that it returns 'true' only if 'filename' ends
    // in either '.jpg' or '.jpeg'
        var array = filename.split(".");
        if(array[1] == "jpg" || array[1] == "jpeg"){
            return true;
        }
        return false;
}

function isWebSizeFriendly(filesize)
{
    // Rewrite this function so that only files with a 'filesize' under
    // 2000000 are considered "web size" friendly (i.e. returns true)
        if(filesize < 2000000){
            return true;
        }
        return false;
}



function generateThumbnailFilename(img_filename)
{
    var img_root = img_filename.substring(0, img_filename.indexOf('.'));
    return "images/thumbnails/" + img_root + "_thumbnail.jpg";
}




function addLinkedImage(divElem, img_filename)
{
    // Complete this function.
    //
    // It should create an HTML image element that is the *thumbnail*
    // version of the 'img_filename' parameter passed in (i.e. use
    // 'generateThumbnailFilename()' above) and append this as a child
    // element of 'divElem'

    var img = document.createElement("img");
    img.setAttribute("src", img_filename);
    divElem.appendChild(img);
}
    

function placeMatch(img_filename,place_list)
{
    // Complete this function.
    //
    // It should check to see if 'img_filename' starts with any of the
    // places in the 'place_list' array (case-insensitive match)
    //
    // If a match occurs then then the matching
    // place name should be return (e.g. "Christchurch") otherwise
    // "Unknown" should be returned
    console.log(img_filename.substring(0, 11));
    var name;
    if(img_filename.substring(0, 12) == place_list[0].toLowerCase()){
        name = place_list[0];
    }
    else if(img_filename.substring(0, 10) == place_list[1].toLowerCase()){
        name = place_list[1];
    }
    else if(img_filename.substring(0, 11) == place_list[2].toLowerCase()){
        name = place_list[2];
    }
    else{
        name = "Unknown";
    }
    return name;
}

var place_list = [ "Christchurch", "Queenstown", "Remarkables" ];


function displayViewer()
{
    // Refer to Question 2 of the test to read about what this top-level
    // JavaScript function implements
    
    var mainviewerDiv = document.getElementById("mainviewer");

    var len = photo_details.length;
    console.log(len);
    
    for (var i = 0; i < len ; i++) {
        var jpg = isWebImageFriendly(photo_details[i].filename);
        var small = isWebSizeFriendly(photo_details[i].filesize);
        var place = placeMatch(photo_details[i].filename, place_list);
        if(jpg && small && place != "Unknown") {
            // var p_elem = document.createElement("p");
            // p_elem.innerHTML = "raw image filename[" + i + "] = " + photo_details[i].filename;
            // mainviewerDiv.appendChild(p_elem);
            var name = generateThumbnailFilename(photo_details[i].filename);
            addLinkedImage(mainviewerDiv, name);
        }
    }
}
